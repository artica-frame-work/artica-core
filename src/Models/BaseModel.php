<?php
declare(strict_types=1);

namespace Artica\Models;

use yii\base\Model;

/**
 * Class BaseModel
 * Base model is exactly a business logical model of system objects.
 *
 * @see \Artica\Models\EntityBaseModel
 *
 * @author  Amin Keshavarz <ak_1596@yahoo.com>
 * @package Artica\Models
 */
abstract class BaseModel extends Model
{

}
