<?php
declare(strict_types=1);

namespace Artica\Forms;

use yii\base\Model;

/**
 * Class Form
 * Base class for Artica Forms.
 *
 * @author  Amin Keshavarz <ak_1596@yahoo.com>
 * @package Artica\Forms
 *
 */
abstract class BaseForm extends Model
{

}
