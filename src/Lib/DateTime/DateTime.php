<?php
declare(strict_types=1);

namespace Artica\Lib\DateTime;

use DateTime as BaseDateTime;

/**
 * Class DateTime
 *
 * @author  Amin Keshavarz <ak_1596@yahoo.com>
 * @package Artica\Lib\DateTime
 */
class DateTime extends BaseDateTime
{
    const FORMAT_STANDARD = 'Y-m-d H:i:s';
}
