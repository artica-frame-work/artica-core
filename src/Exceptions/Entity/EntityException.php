<?php
declare(strict_types=1);

namespace Artica\Exceptions;

use yii\db\Exception;

/**
 * Class EntityException
 * @package Artica\Exceptions
 */
class EntityException extends Exception
{

}